# Umeå university eduroam

These are unofficial instructions to set up eduroam for linux at Umeå university. Instructions for other platforms can be found elsewhere.

I've used the example user John Doe with the umu-id jodo0001 here. You need to change jodo0001 to your own umu-id.

1. Create a directory to keep your certificate files in. I created a (hidden) directory in my home directory called `.eduroam`.
0. Go to https://www.eduroam.umu.se/
0. Create a new certificate.
0. Download the user certificate (`.pfx` file) and the root certificate (`.cer` file) to the directory you created.
0. Save the password in a text file to make setting up eduroam again in the future later easier in case you need to do so.
0. Open the terminal in the directory you created, for example with `cd ~/.eduroam`
0. Run the following command in the terminal, changing out jodo0001 for your own umu-id:
   - `openssl pkcs12 -in jodo0001.pfx -out jodo0001.pem -clcerts`
0. For the following prompts, enter the password you saved previously.
	- `Enter Import Password:`
	- `Enter PEM pass phrase:`
   - `Verifying - Enter PEM pass phrase:`
0. Good job! You should now have a user certificate in `.pem` format. You can check with `ls` in the terminal.

The next steps depend somewhat on the network manager you use but the general idea and settings that need to be configured should be the same. Only the settings described below need to be set, the rest can be left as default.

![Example setup with NetworkManager](https://gitlab.com/toveri/eduroam-umu/raw/master/eduroam.png "Example setup with NetworkManager")

1. Add a new wifi connection.
0. Connection name can be whatever, but `eduroam` works.
0. SSID is `eduroam`
0. Wifi security is `WPA/WPA2 Enterprise`
0. Authentication is `TLS`
0. Identity is `jodo0001@ad.umu.se` (use your own umu-id).
0. User certificate is the `.pem` file you made above.
0. CA certificate is the `.cer` file you downloaded.
0. Private key is the same `.pem` file again.
0. Private key password is the password you saved above.
0. Press OK and you're done! You should now be able to connect to eduroam and use the internet as normal.
